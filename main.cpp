#include <iostream>
#include <sstream>
#include <string>
#include "struct.h"

using namespace std;

void favorite_movie(person *pperson) {

    string mystr;

    cout << "What\'s your favorite movie? ";
    getline(cin, pperson->favorite_movie.GetTitle());

    cout << "Year of the movie: ";
    getline(cin, mystr);
    (stringstream) mystr >> pperson->favorite_movie.GetYear();

}

void person_created(person *pperson) {

    cout << endl << "Person successfully created: " << endl;
    cout << endl;

    printf("Person pointer: %p\n", pperson);
    printf("Person gender: %c\n", pperson->GetGender());
    printf("Person full name: %s", pperson->GetFirstName().c_str());
    printf(" %s\n", pperson->GetLastName().c_str());
    printf("Person first name: %s\n", pperson->GetFirstName().c_str());
    printf("Person last name: %s\n", pperson->GetLastName().c_str());
    printf("Person email: %s\n", pperson->GetEmail().c_str());
    printf("Person age: %d\n\n", pperson->GetAge());

    cout << endl << "Movie research : " << endl;
    cout << endl;

    printf("Person\'s favorite movie : %s\n", pperson->favorite_movie.GetTitle().c_str());
    printf("Year of the movie : %d\n", pperson->favorite_movie.GetYear());
}

void create_person(person *pperson) {

    string mystr;

    cout << "welcome to Movie Enter, enter your personal data:" << endl;
    cout << endl;

    cout << "Enter your Gender: ";
    getline(cin, mystr);
    (stringstream) mystr >> pperson->GetGender();

    cout << "Please, enter your first name: ";
    getline(cin, pperson->GetFirstName());

    cout << "Please, enter your last name: ";
    getline(cin, pperson->GetLastName());

    cout << "Please, enter your email: ";
    getline(cin, pperson->GetEmail());

    cout << "Please, enter your age: ";
    getline(cin, mystr);
    (stringstream) mystr >> pperson->GetAge();

    favorite_movie(pperson);
    person_created(pperson);

}


void person_info(person *pperson) {
    create_person(pperson);
}

int main() {
    person_info(&aperson);
    return 0;
}
