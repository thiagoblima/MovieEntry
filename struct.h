#include <string>

#ifndef STRUCT_H_INCLUDED
#define STRUCT_H_INCLUDED

struct movie {
private:
    int year{};
    std::string title;
public:
    int &GetYear() {
        return year;
    }

    std::string &GetTitle() {
        return title;
    }

    movie() = default;
};

struct person {
private:
    int age{};
    char gender{};
    std::string first_name;
    std::string last_name;
    std::string email;
public:
    movie favorite_movie;

    int &GetAge() {
        return age;
    }

    char &GetGender() {
        return gender;
    }

    std::string &GetFirstName() {
        return first_name;
    }

    std::string &GetLastName() {
        return last_name;
    }

    std::string &GetEmail() {
        return email;
    }

    person() = default;

} aperson{};


#endif // STRUCT_H_INCLUDED
